# currency

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```


### Иллюстрация к проекту
![Иллюстрация к проекту](https://gitlab.com/basov-education/currency/-/raw/main/description/1.png)

![Иллюстрация к проекту](https://gitlab.com/basov-education/currency/-/raw/main/description/2.png)

![Иллюстрация к проекту](https://gitlab.com/basov-education/currency/-/raw/main/description/3.png)