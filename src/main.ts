import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'

import { Quasar, Loading } from 'quasar'
import Ru from './setting/language/ru'
import iconSet from 'quasar/icon-set/fontawesome-v6'
import 'quasar/dist/quasar.css'
import '@quasar/extras/fontawesome-v6/fontawesome-v6.css'

createApp(App).use(Quasar, {
  lang: Ru,
  plugins: [Loading],
  iconSet: iconSet
}).use(store).use(router).mount('#app')
