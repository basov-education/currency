import { ValuteType } from '@/type/valuteType'

export type DailyType = {
    ValuteDate?: Date | string | number | null,
    ValutePreviousDate?: Date | string | number | null,
    ValuteTimestamp?: Date | string | number | null,
    Valute?: ValuteType | null,
}
