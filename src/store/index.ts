import { createStore } from 'vuex'
import axios from 'axios'
import { DailyType } from '@/type/dailyType'
import { ValuteType } from '@/type/valuteType'

export default createStore({
  state: {
    // Валюта
    valute: null as ValuteType | null | undefined,
    valuteTimestamp: null as Date | string | number | null | undefined,
    valutePreviousDate: null as Date | string | number | null | undefined,
    valuteDate: null as Date | string | number | null | undefined
  },
  getters: {
    // Объект - информация о валютах
    valute: (state) => state.valute,
    valuteTimestamp: (state) => state.valuteTimestamp,
    valutePreviousDate: (state) => state.valutePreviousDate,
    valuteDate: (state) => state.valuteDate,
    // Для selec
    codes: (state) => {
      const data = ((Object.values(state.valute ?? [])).map((item) => ({
        value: item.CharCode,
        label: `${item.Name} (${item.CharCode})`
      })))

      // Так как автоматом читает по RUB
      // ТО приходиться таким образом добавлять
      // хотя можно было бы вынести, как отдельный CharCode
      // при условии, что они точно не будут изменяться
      data.push({
        label: 'Российский рубль (RUB)',
        value: 'RUB'
      })

      return data
    },

    // В идеале это тоже должно читаться (в будущем необходимо будет переделать)
    rub: () => ({
      ID: 'G01010',
      NumCode: 1001,
      CharCode: 'RUB',
      Nominal: 1,
      Name: 'Российский рубль',
      Value: 1,
      Previous: 1
    })
  },
  mutations: {
    setDailyJson (state, data: DailyType) {
      state.valute = data.Valute
      state.valuteTimestamp = data.ValuteTimestamp
      state.valutePreviousDate = data.ValutePreviousDate
      state.valuteDate = data.ValuteDate
    }
  },
  actions: {
    // получаем информацию о валютах
    // DailyType - описан тип
    dailyLoad ({ commit }) {
      return new Promise((resolve, reject) => {
        axios({
          url: 'https://www.cbr-xml-daily.ru/daily_json.js',
          method: 'GET'
        })
          .then((resp) => {
            commit('setDailyJson', resp.data)
            resolve(resp)
          })
          .catch((err) => {
            console.warn(err)
            reject(err)
          })
      })
    }
  },
  modules: {
  }
})
