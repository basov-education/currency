import { createRouter, createWebHashHistory, RouteRecordRaw } from 'vue-router'
import СonverterView from '@/views/СonverterView.vue'
import CurrencyListView from '@/views/CurrencyListView.vue'

const routes: Array<RouteRecordRaw> = [
  // Список валют (таблица)
  {
    path: '/',
    name: 'CurrencyList',
    component: CurrencyListView
  },
  // Конвертер валют
  {
    path: '/converter',
    name: 'Сonverter',
    component: СonverterView
  }
]

const router = createRouter({
  history: createWebHashHistory(),
  routes
})

export default router
